#!/bin/sh
DATE=`date +%Y-%m-%d`

### Бэкап
COPYTO=/mnt/yadisk/backup/
mkdir -p /mnt/yadisk/
echo '/mnt/yadisk/ testinggitlab@yandex.ru ecawvhhetolkkfud' >> /etc/davfs2/secrets
mount -t davfs https://webdav.yandex.ru:443 /mnt/yadisk
#echo 'https://webdav.yandex.ru    /mnt/yadisk    davfs    rw,_netdev,uid=root    0    0' >> /etc/fstab
#mount -a
#df -h | grep yandex
wget https://gitlab.com/iakizrouke/scan_docker_util/-/archive/main/scan_docker_util-main.tar.gz -O $COPYTO/$DATE-scan_docker_util.tar.gz
find $COPYTO -mtime +9 -exec rm '{}' \; #Удаляем файлы старше  9 дней
sleep 20s
#For debugging:
#docker run --rm -it --privileged --cap-add=SYS_ADMIN --device /dev/fuse registry.gitlab.com/iakizrouke/scan_docker_util:Backup /bin/sh
