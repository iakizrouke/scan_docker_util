#Install gitlab runner container on linux host
sudo apt-get update && sudo apt-get install docker.io
sudo curl -L --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64
sudo chmod +x /usr/local/bin/gitlab-runner
sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash
sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
sudo gitlab-runner start

sudo gitlab-runner register \
--non-interactive \
--url "https://gitlab.com" \
--registration-token "GR1348941BgwYkzYWZ9rdRhmq9iyg" \
--executor "docker" \
--docker-privileged \
--description "docker-runner" \
--docker-image "docker:19.03.12" \
--tag-list "gitlab-runner" \
--run-untagged="false" \
--docker-volumes /var/run/docker.sock:/var/run/docker.sock \

